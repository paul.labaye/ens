#
# Cibles possibles :
# all: tous les chapitres individuels (chx-yyy.pdf)
# chx-yyy.pdf: un chapitre individuel particulier
# ...
# tout.pdf: document contenant tous les chapitres
# print: tous les PDF au format "3 pages par page"
#

.SUFFIXES:	.pdf .fig .svg .gnu .tex

.fig.pdf:
	fig2dev -L pdf $*.fig $*.pdf

.svg.pdf:
	inkscape --export-pdf=$*.pdf $*.svg

.gnu.pdf:
	gnuplot < $*.gnu > $*.pdf

.tex.pdf:
	pdflatex $*
	pdflatex $*

# pour la cible print
PRINTCMD = pdfjam --quiet --paper a4paper --keepinfo \
	--nup 2x3 --frame true --delta "0.2cm 0.2cm" --scale 0.95

DEPS	= courspda.sty casserole.pdf logo-uds.pdf \
	annee.tex \
	licence.tex by-nc.pdf

##############################################################################
# Cours 1

SRC1 = ch1.tex sl1.tex

FIG1 = \
	inc/spool1.pdf \
	inc/spool2.pdf \
	inc/quantum.pdf \
	inc/noyau.pdf \

IMG1 = \
	inc/eniac.jpg \
	inc/pdp1.jpg \
	inc/carte-perfo.jpg \
	inc/ibm704.jpg \
	inc/spool-tanenb.png \
	inc/term-tty.jpg \
	inc/term-adm3a.jpg \
	inc/pdp11.jpg \

LST1 = \
	inc/open.py \

##############################################################################
# Cours 2

SRC2 = ch2.tex sl2.tex

FIG2 = \
	inc/arbo.pdf \
	inc/droits.pdf \
	inc/arbo-impl.pdf \
	inc/redir0.pdf \
	inc/redir1.pdf \
	inc/redir2.pdf \
	inc/redir3.pdf \
	inc/redir4.pdf \
	inc/tube2.pdf \
	inc/tube3.pdf \
	inc/anat-proc.pdf \
	inc/shift.pdf \

IMG2 = \
	inc/term-tty.jpg \
	inc/term-vt100.jpg \
	inc/term-kde.jpg \

LST2 = \
	inc/sh-ls.out \
	inc/sh-ls-l.out \
	inc/sh-stat.out \
	inc/synt-if.sh \
	inc/synt-while.sh \
	inc/synt-until.sh \
	inc/ex-rm.sh \
	inc/ex-test.sh \
	inc/synt-for-in.sh \
	inc/synt-for.sh \
	inc/ex-for.sh \
	inc/ex-case.sh \
	inc/ex-find1.sh \
	inc/ex-find2.sh \
	inc/ex-mail1.sh \
	inc/ex-mail2.sh \


##############################################################################
# L'ensemble

SRCall = \
	$(SRC1) \
	$(SRC2) \
	tout.tex

FIGall = \
	$(FIG1) \
	$(FIG2) \

IMGall = \
	$(IMG1) \
	$(IMG2) \

LSTall = \
	$(LST1) \
	$(LST2) \

##############################################################################
# Les cibles
##############################################################################

all:	ch1.pdf \
	ch2.pdf \

ch1.pdf:  $(DEPS) $(FIG1) $(IMG1) $(LST1) $(SRC1)
ch2.pdf:  $(DEPS) $(FIG2) $(IMG2) $(LST2) $(SRC2)

inc/redir0.pdf: inc/redir.fig
	./figlayers 40 45-99 < $< | fig2dev -L pdf /dev/stdin $@
inc/redir1.pdf: inc/redir.fig		# sort > ...
	./figlayers 35-39 45-99 < $< | fig2dev -L pdf /dev/stdin $@
inc/redir2.pdf: inc/redir.fig		# sort < ...
	./figlayers 30-34 45-99 < $< | fig2dev -L pdf /dev/stdin $@
inc/redir3.pdf: inc/redir.fig		# sort 2> ...
	./figlayers 25-29 45-99 < $< | fig2dev -L pdf /dev/stdin $@
inc/redir4.pdf: inc/redir.fig		# sort < ... > ... 2> ...
	./figlayers 20 26-29 31-34 36-39 45-99 < $< | fig2dev -L pdf /dev/stdin $@

tout.pdf:	$(DEPS) $(FIGall) $(LSTall) $(SRCall)

print:	print-tout.pdf

print-tout.pdf: tout.pdf
	$(PRINTCMD) -o print-tout.pdf tout.pdf

clean:
	rm -f $(FIGall)
	rm -f *.bak */*.bak *.nav *.out *.snm *.vrb *.log *.toc *.aux
	rm -f print-*.pdf ch*.pdf tout*.pdf by-nc.pdf casserole.pdf
	rm -f inc*/a.out
