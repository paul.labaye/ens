verrouiller:			// argument dans la pile
	ld   [%sp+1],%a		// a $\leftarrow$ adresse du verrou
	push %b			// on va utiliser b, le sauvegarder d'abord
boucle:
	ld   [%a],%b		// b $\leftarrow$ *verrou
        cmp  0,%b		// teste la valeur de b
	jne  boucle		// boucle si b $\neq$ 0
        ld   1,%b
	st   %b,[%a]		// *verrou $\leftarrow$ 1
	pop  %b			// restaurer b
        rtn
