void sleep (void *wchan) {
  curproc->etat = ATTENTE ;
  curproc->wchan = wchan ;
  swtch (...) ;			// provoquer la commutation maintenant !
  // on arrive ici après qu'un autre processus ait fait wakeup(...)
}

void wakeup (void *wchan) {	// utiliser une section critique si multiprocesseur
  for (int i = 0 ; i < NPROC ; i++) {
    if (proc[i].etat == ATTENTE && proc[i].wchan == wchan) {
      proc[i].etat = PRET_A_TOURNER ;
      commuter = 1 ;		// demander une commutation au retour en mode utilisateur
    }
  }
}

void lire_dans_tube (struct inode *ip) {
  while (tube_vide (ip)) {	// \frquote{while} car au réveil, un autre processus peut avoir déjà
    sleep (ip) ;		// tout lu dans le tube
  }
  // écrire dans le tube
}

void ecrire_dans_tube (struct inode *ip) {
  // écrire dans le tube (en ayant vérifié qu'il n'est pas plein !)
  wakeup (ip) ;			// on a écrit dedans, donc le tube n'est plus vide
}
